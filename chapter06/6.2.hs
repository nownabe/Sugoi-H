import qualified Data.Char
import qualified Data.List

wordNums :: String -> [(String, Int)]
wordNums = map (\ws -> (head ws, length ws)) . Data.List.group . Data.List.sort . Data.List.words

isIn :: (Eq a) => [a] -> [a] -> Bool
needle `isIn` haystack = any (needle `Data.List.isPrefixOf`) (Data.List.tails haystack)

encode :: Int -> String -> String
encode offset = map (Data.Char.chr . (+offset) . Data.Char.ord)

decode :: Int -> String -> String
decode offset = map (Data.Char.chr . subtract offset . Data.Char.ord)

digitSum :: Int -> Int
digitSum = sum . map Data.Char.digitToInt . show

firstTo40 :: Maybe Int
firstTo40 = firstTo 40

firstTo :: Int -> Maybe Int
firstTo n = Data.List.find (\x -> digitSum x == n) [1..]
